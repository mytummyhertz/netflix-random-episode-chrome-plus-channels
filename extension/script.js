var seasonsData = [],
    loadingDone = false;

var randomBtn = $('<span id="randomBtn" class="btnWrap mltBtn mltBtn-s60"><a href="#" class="btn btn-60 btn-ED-60"><span class="inr">Random Episode</span></a></span>');

var randomBOB = $('<div class="episodeBOB leftBOB densityEpisodeBOB" style="display: none; visibility: visible; top: -65px; left: -330px;" />')
                    .append('<span class="ebob-arrow transp nse" style="top: 52px; height:52px; left:251px;" />')
                    .append('<div class="ebob-head transp" />')
                    .append('<div class="ebob-content transp" />')
                    .append('<div class="ebob-foot transp" />');

var throbber = $('<div class="randomLoading" />')
                    .css('background-image','url(' + chrome.extension.getURL("images/throbber.gif") + ')');

var loadingScr = $('<div class="ebob-content transp" />')
                    .append('<h3 class="heading"> Loading Episodes </h3>')
                    .append(throbber);
                    
var loadingBOB = $('<div />')
                    .append(loadingScr)
                    .append('<a href="#" />');

getRandomEpisode = function() {
    if(loadingDone) {
        var ep = seasonsData[Math.floor(Math.random() * seasonsData.length)];
        replaceBob($(ep));
    }

    //check if we have the go-flag
    var goFlag = checkGoFlag();
 //   console.log("goflag:" + goFlag);
    if(goFlag){
        //if so, call link on randomBtn
        var toGo = randomBtn.find('a').attr('href');
  //      console.log("going to go to:" + toGo);
        window.location = toGo;
    } 
}

loadEpisodes = function(seriesHref) {
    var seasons = null;
    
    if(seriesHref) {
        // series href given
        $.get(seriesHref, function (data) {
            seasons = $(data).find("#seasonsNav .seasonItem a");
        });
    } else {
        // assume the current window location is the series page
        seriesHref = window.location.href.replace("#","");
        seasons = $("#seasonsNav .seasonItem a");
    }
    
    // show loading screen
    replaceBob(loadingBOB);
    
    if(seasons.length) {
        // multiple seasons listed, load each tab and get the episodes
        var numLoaded = 0;
        
        seasons.each(function(i,e) {
            $.getJSON(seriesHref + "&actionMethod=seasonDetails&seasonId=" + 
                      $(e).attr("data-vid") + "&seasonKind=ELECTRONIC", 
                      function(data,textStatus) {
                numLoaded += 1;
                
                $(data.html).find(".episodeList li").each(function(i,e) {
                    seasonsData.push(e);
                });
                
                if(numLoaded == seasons.length) {
                    loadingDone = true;
                    getRandomEpisode();
                }
            });
        });
    } else {
        // just one season, no need to load more tabs
        $("#seasonDetail").find(".episodeList li").each(function(i,e) {
            seasonsData.push(e);
        });
        
        loadingDone = true;
        getRandomEpisode();
    }
}

replaceBob = function(newBob) {
    randomBOB.find('.ebob-content').html('').append(newBob.find('.ebob-content').html());
    randomBtn.find('a.btn').attr('href',newBob.find('a').attr('href'));
}


var baseurl = "http://movies.netflix.com/WiMovie/";
var ids = ["70136124", "70136120"];
var suffix = "?go=true";

goToID = function(id){
    window.location = baseurl + id + suffix;
}

// at some point, may be worth writing a better PRNG, as js's is pretty shitastic
watchNextOnChannel = function() {
 //   console.log("ids len:" + ids.length);

    randNum = Math.random();
    randID = Math.floor(randNum * (ids.length));   
 //   console.log("rand:" + randNum + " mul:" + randNum * ids.length + " chosen:" + randID);
    theID = ids[randID];
    goToID(theID);
}




parseID = function(){
    var longName = window.location.pathname;
    //console.log("longname: " + longName);
    var pieces = longName.split("/");
  //  console.log("pieces:" + pieces);
    for(i=0; i < pieces.length; i++){
        piece = pieces[i];
  //      console.log("piece:" + piece);
        if(piece.indexOf("?") != -1) piece = piece.split("?")[0];
        if(!isNaN(piece) && piece != "") 
            return piece;
    }
    return "couldn't find id";
}


//add the new ID to ids and saves it to storage
addToChannel = function(){
    console.log("ids before:" + ids.toString());
    id = parseID();
    if( ids.indexOf(id) == -1)
        ids.push(id);
    console.log("ids after:" + ids.toString());
}

//saves the ids into storage
saveIDs = function(){
    chrome.storage.local.set({"ids": ids });
}

removeFromChannel = function(){
    console.log("ids before:" + ids.toString());
    id = parseID();
    var idx = ids.indexOf(id); // Find the index
    if( idx != - 1)
        ids.splice(idx, 1); // Remove it if really found!
    console.log("ids after:" + ids.toString());
}

checkGoFlag = function(){
    addr = window.location.href;
    var loc = addr.indexOf("?go");
    if(loc == -1) return false;
        return true;
}

changeButton = function(){
   // console.log("changebutton called!");
    if( $("#displaypage-overview-details").find(".actions").find(addToChannelButton).length) {// add -> remove
     //   console.log("found add button");
        $("#addToChannelButton").remove();
        $("#displaypage-overview-details").find(".actions").append(removeFromChannelButton);
    } 
    else if( $("#displaypage-overview-details").find(".actions").find(removeFromChannelButton).length) { // remove -> add
    //    console.log("found remove button");
        $("#removeFromChannelButton").remove()
        $("#displaypage-overview-details").find(".actions").append(addToChannelButton);
    }
    //change the button from addtochannel -> removefromchannel or vice versa
}

var watchNextButton = $('<a id="watchNextButton" href="javascript:window.postMessage({ type:&quot;watchnext&quot;, text:&quot;watchnext&quot; }, &quot;*&quot;);" class="svf-button svfb-default mediaKindToggle svf-button-right svfb-blue" />')
                    .append('<span class="inr">Watch Next on Channel</span>');

var addToChannelButton = $('<a id="addToChannelButton" href="javascript:window.postMessage({ type:&quot;addtochannel&quot;, text:&quot;addtochannel&quot; }, &quot;*&quot;);" class="svf-button svfb-default mediaKindToggle svf-button-right svfb-blue" />')
                    .append('<span class="inr">Add to Channel</span>');

var removeFromChannelButton = $('<a id="removeFromChannelButton" href="javascript:window.postMessage({ type:&quot;removefromchannel&quot;, text:&quot;removefromchannel&quot; }, &quot;*&quot;);" class="svf-button svfb-default mediaKindToggle svf-button-right svfb-blue" />')
                    .append('<span class="inr">Remove from Channel</span>');

 

$(function(){
    // quit if not episodic
    var duration = $("#displaypage-overview-details").find(".duration").html();
    if(!(duration.search("Episode") || duration.search("Season"))) {
        return; 
    }
    
    // append bob and button to bodyTop
    $("#displaypage-overview-details").find(".actions").append(randomBtn.append(randomBOB.hide()));
    randomBtn.find(".btn").css("background-image",'url(' + chrome.extension.getURL("images/play.png") + ')');

    //load ids from chrome storage
    var theIDs = chrome.storage.local.get("ids", function(theIDs){
        console.log("ids beforehand: " + ids.toString());
        if(theIDs.ids)
            ids = theIDs.ids;
        console.log("ids afterwards: " + ids.toString());
        //append new buttons
        $("#displaypage-overview-details").find(".actions").append(watchNextButton);
        //check whether page is already in channel, display correct button
        if(ids.indexOf(parseID()) == -1)
            $("#displaypage-overview-details").find(".actions").append(addToChannelButton);
        else
            $("#displaypage-overview-details").find(".actions").append(removeFromChannelButton);
    });



    // set up button hover
    randomBtn.hover(function(e) {
        getRandomEpisode();
        randomBOB.show();
    }, function(e) {
        randomBOB.hide();
    });
    


    //set up listener
    var port = chrome.extension.connect();
    window.addEventListener("message", function(event) {
       // console.log("im an anonymous listener!")
        // We only accept messages from ourselves
        if (event.source != window){
          return;
        }
        if (event.data.type){ // we have a real message
            if(event.data.type == "watchnext"){
                watchNextOnChannel();
            }else
            if(event.data.type == "addtochannel"){
                //  console.log("im stuck here");
                addToChannel();
                changeButton();
                saveIDs();
            }else
            if(event.data.type == "removefromchannel"){
                removeFromChannel();
                changeButton();
                saveIDs();
            }
        }
    }, false);

    // load episodes
    loadEpisodes();
});
